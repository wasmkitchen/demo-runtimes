# Demo RunTimes

## Install Swift support

```bash
#SWIFT_VERSION="5.6.0"
SWIFT_VERSION="5.7.1"
#SWIFT_ARCH="ubuntu20.04_x86_64"
SWIFT_ARCH="macos_arm64"
#ASSET_EXTENSION="tar.gz"
ASSET_EXTENSION="pkg"

cd $HOME
wget "https://github.com/swiftwasm/swift/releases/download/swift-wasm-${SWIFT_VERSION}-RELEASE/swift-wasm-${SWIFT_VERSION}-RELEASE-${SWIFT_ARCH}.${ASSET_EXTENSION}"

# if linux
tar xzf swift-wasm-${SWIFT_VERSION}-RELEASE-${SWIFT_ARCH}.${ASSET_EXTENSION}
export PATH=$(pwd)/swift-wasm-${SWIFT_VERSION}-RELEASE/usr/bin:"${PATH}"

# if macos
sudo installer -pkg swift-wasm-${SWIFT_VERSION}-RELEASE-${SWIFT_ARCH}.${ASSET_EXTENSION} -target /
export PATH=/Library/Developer/Toolchains/swift-latest.xctoolchain/usr/bin:"${PATH}"

swift --version

rm swift-wasm-${SWIFT_VERSION}-RELEASE-${SWIFT_ARCH}.${ASSET_EXTENSION}
```

## Install Grain support

```bash
brew install --no-quarantine --cask grain-lang/tap/grain
```