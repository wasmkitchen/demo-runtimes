#!/bin/bash
export PATH=/Library/Developer/Toolchains/swift-latest.xctoolchain/usr/bin:"${PATH}"

swiftc -target wasm32-unknown-wasi hello.swift -o hello.wasm

ls -lh *.wasm