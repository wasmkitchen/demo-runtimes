# WASI & Go

```bash
tinygo build -o main.wasm -target wasi ./main.go

wasmer main.wasm 👋 good morning Bob
wasmedge main.wasm 👋 good morning Bob
wasmtime main.wasm 👋 good morning Bob
```
